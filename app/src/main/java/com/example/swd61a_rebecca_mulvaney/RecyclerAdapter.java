package com.example.swd61a_rebecca_mulvaney;

import android.app.LauncherActivity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private ArrayList<RecylcerViewData> rvData;
    private Context context;

    public RecyclerAdapter(ArrayList<RecylcerViewData> rvData, Context context) {
        this.rvData = rvData;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.ViewHolder viewHolder, int i) {
        RecylcerViewData ra = rvData.get(i);
        String imageURL = ra.getImageUrl();
        String artistName = ra.getArtistName();
        int likes = ra.getNoOfLikes();

        Picasso.with(context).load(imageURL).fit().centerInside().into(viewHolder.iView);
        viewHolder.tViewArtist.setText(artistName);
        viewHolder.tViewLikes.setText("Likes: " + likes);
    }

    @Override
    public int getItemCount() {
        return rvData.size();
    }

    //instance of ViewHolder
    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView iView;
        public TextView tViewArtist;
        public TextView tViewLikes;

        public ViewHolder(View itemView) {
            super(itemView);
            iView = itemView.findViewById(R.id.cardImage);
            tViewArtist = itemView.findViewById(R.id.textArtist);
            tViewLikes = itemView.findViewById(R.id.textLikes);
        }
    }


}
