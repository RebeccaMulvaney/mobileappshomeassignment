package com.example.swd61a_rebecca_mulvaney;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginOrRegister extends AppCompatActivity {

    Button loginBtn;
    Button registerBtn;

    private EditText email;
    private EditText pwd;


    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navigationView;

    //Declaring Firebase Authentication
    FirebaseAuth fAuth;
    FirebaseAuth.AuthStateListener asl;
    private SignInButton googleBtn;
    private static final int RC_SIGN_IN = 1;
    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_or_register);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open_drawer, R.string.close_drawer);

        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.action_home:
                        Intent intent = new Intent(LoginOrRegister.this, MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_explore:
                        Intent intent2 = new Intent(LoginOrRegister.this, Main2Activity_explore.class);
                        startActivity(intent2);
                        break;
                    case R.id.action_account:
                        Intent intent3 = new Intent(LoginOrRegister.this, LoginOrRegister.class);
                        startActivity(intent3);
                        break;
                }
                return true;
            }
        });


        email = (EditText) findViewById(R.id.emailText);
        pwd = (EditText) findViewById(R.id.pwdText);
        loginBtn = findViewById(R.id.loginBtn);
        registerBtn = findViewById(R.id.signUpBtn);

        fAuth = FirebaseAuth.getInstance();
        asl = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser() != null){
                    Intent am3 = new Intent(LoginOrRegister.this, Main3Activity_account.class);
                    startActivity(am3);
                }
            }
        };

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailText = email.getText().toString();
                String pwdText = pwd.getText().toString();
                fAuth.signInWithEmailAndPassword(emailText,pwdText).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Intent i = new Intent(LoginOrRegister.this, Main3Activity_account.class);
                            startActivity(i);
                        }
                        else{
                            Toast.makeText(LoginOrRegister.this, "Email or Password incorrect. Please try again!",Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailText = email.getText().toString();
                String pwdText = pwd.getText().toString();
                fAuth.createUserWithEmailAndPassword(emailText, pwdText).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(LoginOrRegister.this, "Sign Up Successful",Toast.LENGTH_LONG).show();
                        }
                        else{
                            Toast.makeText(LoginOrRegister.this, "Sign Up was NOT Successful",Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }
}
