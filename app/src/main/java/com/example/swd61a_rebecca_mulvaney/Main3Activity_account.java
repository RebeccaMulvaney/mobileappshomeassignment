package com.example.swd61a_rebecca_mulvaney;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class Main3Activity_account extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navigationView;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerAdapter;
    private ArrayList<RecylcerViewData> rvData;

    private FirebaseAuth fAuth;
    DatabaseReference dbRef;
    StorageReference sRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3_account);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open_drawer, R.string.close_drawer);

        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.action_home:
                        Intent intent = new Intent(Main3Activity_account.this, MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_explore:
                        Intent intent2 = new Intent(Main3Activity_account.this, Main2Activity_explore.class);
                        startActivity(intent2);
                        break;
                    case R.id.action_account:
                        Intent intent3 = new Intent(Main3Activity_account.this, Main3Activity_account.class);
                        startActivity(intent3);
                        break;
                }
                return true;
            }
        });

        BottomNavigationView bNV = (BottomNavigationView) findViewById(R.id.bottom_nav_menu);
        bNV.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.add_photo:
                        Intent i = new Intent(Main3Activity_account.this, ImageCutomisation.class);
                        startActivity(i);
                        break;
                    case R.id.remove_photo:
                        Intent i2 = new Intent(Main3Activity_account.this, ImageCutomisation.class);
                        startActivity(i2);
                        break;
                    case R.id.edit_caption:
                        Intent i3 = new Intent(Main3Activity_account.this, ImageCutomisation.class);
                        startActivity(i3);
                        break;
                }
                return true;
            }
        });

        fAuth = FirebaseAuth.getInstance();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        rvData = new ArrayList<>();
        //showItemsInStorage();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(drawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
/* --Does not work--
    public void showItemsInStorage(){
        FirebaseUser user = fAuth.getCurrentUser();
        final String userid = user.getUid().toString();

        dbRef = FirebaseDatabase.getInstance().getReference("images/users/"+userid);

        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    RecylcerViewData rvd = ds.getValue(RecylcerViewData.class);
                    rvData.add(rvd);
                }
                recyclerAdapter = new RecyclerAdapter(rvData, Main3Activity_account.this);
                recyclerView.setAdapter(recyclerAdapter);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(Main3Activity_account.this, ""+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    */
}
