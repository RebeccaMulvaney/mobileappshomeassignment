package com.example.swd61a_rebecca_mulvaney;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class PinView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_lock);


        Pinview pinview = (Pinview) findViewById(R.id.pinview);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                if(pinview.getValue().equals("1232")){
                    Intent i = new Intent(PinView.this, MainActivity.class);
                    startActivity(i);
                    Toast.makeText(PinView.this,"Welcome to Artudio", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
