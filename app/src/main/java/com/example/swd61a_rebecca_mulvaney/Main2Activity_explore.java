package com.example.swd61a_rebecca_mulvaney;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Main2Activity_explore extends AppCompatActivity {

    //for drawer navigation
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navigationView;

    //for recycler view
    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerAdapter;
    private ArrayList<RecylcerViewData> rvData;
    private RequestQueue rQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2_explore);

        //start of navigation sidebar code
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open_drawer, R.string.close_drawer);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.action_home:
                        Intent intent = new Intent(Main2Activity_explore.this, MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_explore:
                        Intent intent2 = new Intent(Main2Activity_explore.this, Main2Activity_explore.class);
                        startActivity(intent2);
                        break;
                    case R.id.action_account:
                        Intent intent3 = new Intent(Main2Activity_explore.this, LoginOrRegister.class);
                        startActivity(intent3);
                        break;
                }
                return true;
            }
        });
        //end of navigation sidebar code

        //start of recycler view code
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        rvData = new ArrayList<>();
        rQueue = Volley.newRequestQueue(this);

        //end of recycler view code

    }

    public void onClickBtn(View v){
        rvData.clear();
        TextInputEditText searchInput = (TextInputEditText) findViewById(R.id.edit_text_input);
        String content = searchInput.getText().toString();
        parseJSONData("https://pixabay.com/api/?key=12656235-5841fbe56013c1bf7c47dbca2&q=" + content);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(drawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void parseJSONData(String url){

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jArray = response.getJSONArray("hits");
                            for(int i = 0; i < jArray.length(); i++){
                                JSONObject hits = jArray.getJSONObject(i);
                                String artist = hits.getString("user");
                                String imgUrl = hits.getString("webformatURL");
                                int likes = hits.getInt("likes");

                                //filling the array list with new data by calling a new instance of the recyclerViewData class
                                rvData.add(new RecylcerViewData(imgUrl,artist,likes));
                            }

                            recyclerAdapter = new RecyclerAdapter(rvData, Main2Activity_explore.this);
                            recyclerView.setAdapter(recyclerAdapter);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        rQueue.add(jsonRequest);
    }
}
