package com.example.swd61a_rebecca_mulvaney;

public class RecylcerViewData {

    private String imageUrl;
    private String artistName;
    private int noOfLikes;

    public RecylcerViewData(String iUrl, String aName, int likes) {
        this.imageUrl = iUrl;
        this.artistName = aName;
        this.noOfLikes = likes;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getArtistName() {
        return artistName;
    }

    public int getNoOfLikes() {
        return noOfLikes;
    }
}
