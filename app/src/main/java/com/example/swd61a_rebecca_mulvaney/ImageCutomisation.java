package com.example.swd61a_rebecca_mulvaney;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

public class ImageCutomisation extends AppCompatActivity {

    Button next;
    Button upload;
    Button remove;
    EditText caption;
    ImageView imageView;

    private FirebaseAuth fAuth;
    FirebaseStorage fStorage;
    StorageReference sRef;
    DatabaseReference dbRef;
    int count = 1;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_cutomisation);

        next = (Button) findViewById(R.id.nextBtn);
        upload = (Button) findViewById(R.id.addBtn);
        remove = (Button) findViewById(R.id.removeBtn);
        caption = (EditText) findViewById(R.id.imgCaption);
        imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.illustration_1);

        fAuth = FirebaseAuth.getInstance();
        fStorage = FirebaseStorage.getInstance();
        sRef = fStorage.getReference();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                switch (count){
                    case 2:
                        imageView.setImageResource(R.drawable.illustration_2);
                        uri = Uri.parse("android.resource://SWD61A_Rebecca_Mulvaney/" + R.drawable.illustration_2);
                        break;
                    case 3:
                        imageView.setImageResource(R.drawable.illustration_3);
                        uri = Uri.parse("android.resource://SWD61A_Rebecca_Mulvaney/" + R.drawable.illustration_3);
                        break;
                    case 4:
                        imageView.setImageResource(R.drawable.illustration_4);
                        uri = Uri.parse("android.resource://SWD61A_Rebecca_Mulvaney/" + R.drawable.illustration_4);
                        break;
                    case 5:
                        imageView.setImageResource(R.drawable.illustration_5);
                        uri = Uri.parse("android.resource://SWD61A_Rebecca_Mulvaney/" + R.drawable.illustration_5);
                        break;
                    case 6:
                        imageView.setImageResource(R.drawable.illustration_6);
                        uri = Uri.parse("android.resource://SWD61A_Rebecca_Mulvaney/" + R.drawable.illustration_6);
                        break;
                    case 7:
                        imageView.setImageResource(R.drawable.illustration_7);
                        uri = Uri.parse("android.resource://SWD61A_Rebecca_Mulvaney/" + R.drawable.illustration_7);
                        break;
                    case 8:
                        imageView.setImageResource(R.drawable.illustration_8);
                        uri = Uri.parse("android.resource://SWD61A_Rebecca_Mulvaney/" + R.drawable.illustration_8);
                        break;
                    case 9:
                        imageView.setImageResource(R.drawable.illustration_9);
                        uri = Uri.parse("android.resource://SWD61A_Rebecca_Mulvaney/" + R.drawable.illustration_9);
                        count = 0;
                        break;
                }
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadPhoto();
                startActivity(new Intent(ImageCutomisation.this, Main3Activity_account.class));
            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePhoto();
                startActivity(new Intent(ImageCutomisation.this, Main3Activity_account.class));
            }
        });
    }

    public void uploadPhoto(){
        FirebaseUser fuser = fAuth.getCurrentUser();
        final String userId = fuser.getUid().toString();

        dbRef = FirebaseDatabase.getInstance().getReference("images/users/"+ userId + "/");
        final String name = caption.getText().toString();
        if(!name.equals("")){
            final StorageReference sr = sRef.child("images/users/" + userId + "/" + name + ".jpg");

            Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            final byte[] data = baos.toByteArray();

            final UploadTask uTask = sr.putBytes(data);
            uTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(ImageCutomisation.this, "Upload unsuccessful", Toast.LENGTH_LONG).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    RecylcerViewData rvd = new RecylcerViewData(sr.getDownloadUrl().toString(),null,0);
                    String uploadId = dbRef.push().getKey();
                    dbRef = FirebaseDatabase.getInstance().getReference("images/users/"+userId);
                    dbRef.child(uploadId).setValue(rvd);
                }
            });
            Toast.makeText(ImageCutomisation.this, "Upload successful", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(ImageCutomisation.this, "Please enter a caption for the image.", Toast.LENGTH_LONG).show();
        }
    }

    public void removePhoto(){
        FirebaseUser fuser = fAuth.getCurrentUser();
        String userId = fuser.getUid().toString();

        String name = caption.getText().toString();

        StorageReference sr = sRef.child("images/users/" + userId + "/" + name + ".jpg");
        sr.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(ImageCutomisation.this, "File deleted successfully.", Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(ImageCutomisation.this, "Error deleting file." + e, Toast.LENGTH_LONG).show();
            }
        });

    }
}
